// GENERATED AUTOMATICALLY FROM 'Assets/Data/Input/InputMaster.inputactions'

using System;
using UnityEngine;
using UnityEngine.Experimental.Input;


[Serializable]
public class InputMaster : InputActionAssetReference
{
    public InputMaster()
    {
    }
    public InputMaster(InputActionAsset asset)
        : base(asset)
    {
    }
    private bool m_Initialized;
    private void Initialize()
    {
        // Player
        m_Player = asset.GetActionMap("Player");
        m_Player_Interact = m_Player.GetAction("Interact");
        m_Player_MainAction = m_Player.GetAction("MainAction");
        m_Player_SecondAction = m_Player.GetAction("SecondAction");
        m_Player_Tablet = m_Player.GetAction("Tablet");
        m_Player_Light = m_Player.GetAction("Light");
        m_Player_Movement = m_Player.GetAction("Movement");
        m_Player_Jump = m_Player.GetAction("Jump");
        m_Initialized = true;
    }
    private void Uninitialize()
    {
        m_Player = null;
        m_Player_Interact = null;
        m_Player_MainAction = null;
        m_Player_SecondAction = null;
        m_Player_Tablet = null;
        m_Player_Light = null;
        m_Player_Movement = null;
        m_Player_Jump = null;
        m_Initialized = false;
    }
    public void SetAsset(InputActionAsset newAsset)
    {
        if (newAsset == asset) return;
        if (m_Initialized) Uninitialize();
        asset = newAsset;
    }
    public override void MakePrivateCopyOfActions()
    {
        SetAsset(ScriptableObject.Instantiate(asset));
    }
    // Player
    private InputActionMap m_Player;
    private InputAction m_Player_Interact;
    private InputAction m_Player_MainAction;
    private InputAction m_Player_SecondAction;
    private InputAction m_Player_Tablet;
    private InputAction m_Player_Light;
    private InputAction m_Player_Movement;
    private InputAction m_Player_Jump;
    public struct PlayerActions
    {
        private InputMaster m_Wrapper;
        public PlayerActions(InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact { get { return m_Wrapper.m_Player_Interact; } }
        public InputAction @MainAction { get { return m_Wrapper.m_Player_MainAction; } }
        public InputAction @SecondAction { get { return m_Wrapper.m_Player_SecondAction; } }
        public InputAction @Tablet { get { return m_Wrapper.m_Player_Tablet; } }
        public InputAction @Light { get { return m_Wrapper.m_Player_Light; } }
        public InputAction @Movement { get { return m_Wrapper.m_Player_Movement; } }
        public InputAction @Jump { get { return m_Wrapper.m_Player_Jump; } }
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled { get { return Get().enabled; } }
        public InputActionMap Clone() { return Get().Clone(); }
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
    }
    public PlayerActions @Player
    {
        get
        {
            if (!m_Initialized) Initialize();
            return new PlayerActions(this);
        }
    }
    private int m_KeyboardAndMouseSchemeIndex = -1;
    public InputControlScheme KeyboardAndMouseScheme
    {
        get

        {
            if (m_KeyboardAndMouseSchemeIndex == -1) m_KeyboardAndMouseSchemeIndex = asset.GetControlSchemeIndex("KeyboardAndMouse");
            return asset.controlSchemes[m_KeyboardAndMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get

        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.GetControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
}
