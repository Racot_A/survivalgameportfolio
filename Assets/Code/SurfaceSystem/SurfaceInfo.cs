﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/SurfaceSystem/Surface Info")]
public class SurfaceInfo : ScriptableObject
{
    public PhysicMaterial Material;
}
