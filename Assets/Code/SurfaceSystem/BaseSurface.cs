﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseSurface : MonoBehaviour, ISurface
{
    [SerializeField] protected SurfaceInfo SurfaceInfo;

    public SurfaceInfo Info => SurfaceInfo;
}
