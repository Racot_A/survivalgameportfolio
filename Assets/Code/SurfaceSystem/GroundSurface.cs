﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSurface : BaseSurface
{
    private void Awake()
    {
        var coll = GetComponent<Collider>();
        if (coll)
        {
            coll.material = Info.Material;
        }
    }
}
