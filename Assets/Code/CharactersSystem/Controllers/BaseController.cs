﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    public event Action MakeFootstep;

    private StaminaStat _stamina;

    private void Awake()
    {
        PostAwake();
    }
    
    private void Start()
    {
        PostStart();
    }

    protected virtual void PostAwake()
    {
        
    }
    
    protected virtual void PostStart()
    {
        _stamina = GetComponent<CharacterStats>().GetStat<StaminaStat>();
    }

    

    public void OnStep()
    {
        MakeFootstep?.Invoke();
    }
    
    public virtual bool IsRunning()
    {
        return false;
    }
    
    public virtual bool CanRunning()
    {
        return _stamina.Value > 0;
    }
    
}
