﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : BaseController
{
    private FirstPersonController _firstPersonController;

    protected override void PostAwake()
    {
        
        base.PostAwake();
        
        _firstPersonController = GetComponent<FirstPersonController>();
    }

    public override bool IsRunning()
    {
        return !_firstPersonController.IsWalking;
    }
}
