﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FootstepSurface
{
    public SurfaceInfo SurfaceInfo;
    [BundleAssetReference]
    public string[] FootstepSounds;
}

public class CharacterFootsteps : MonoBehaviour
{
    [SerializeField, Range(0.1f, 3f)]
    protected float CheckSurfaceDistance = 3;
    [SerializeField]
    protected LayerMask CheckSurfaceMask;
    [SerializeField]
    protected List <FootstepSurface> FootstepSurfaces = new List<FootstepSurface>();
    
    [SerializeField, Header("[SFX]")]
    protected AudioSource FootstepSource;

    private FootstepSurface _lastStepSurface;
    private BaseController _controller;
    private RaycastHit _raycastHit;
    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<BaseController>();
        _controller.MakeFootstep += ControllerOnMakeFootstep;
    }

    private void OnDestroy()
    {
        _controller.MakeFootstep -= ControllerOnMakeFootstep;
    }

    private void ControllerOnMakeFootstep()
    {
        if (FootstepSurfaces.Count <= 0)
        {
            return;
        }
        CheckSurface();
    }

    private void CheckSurface()
    {
        var ray = new Ray(transform.position, Vector3.down * CheckSurfaceDistance);
        var castSomething = Physics.Raycast(ray, out _raycastHit, CheckSurfaceDistance, CheckSurfaceMask);
        Debug.DrawRay(ray.origin, ray.direction);
        if (castSomething)
        {
            var surface = _raycastHit.transform.GetComponent<ISurface>();
            //Debug.Log($"[SURFACES SYSTEM] Surface:{surface.GetType()}");
            PlayFootStepAudio(surface.Info);
        }
    }
    
    private void PlayFootStepAudio(SurfaceInfo surfaceInfo)
    {
        FootstepSurface curSurface = null;
        if (_lastStepSurface != null && _lastStepSurface.SurfaceInfo == surfaceInfo)
        {
            curSurface = _lastStepSurface;
        }
        else
        {
            _lastStepSurface = FootstepSurfaces.Find(x => x.SurfaceInfo.Equals(surfaceInfo));
        }

        if (curSurface == null)
        {
            return;
        }

        int n = Random.Range(1, curSurface.FootstepSounds.Length);
        var clip  = curSurface.FootstepSounds[n];
        
        UnityAudioPlayer.PlayOneShoot(curSurface.FootstepSounds[n], FootstepSource);
        
        curSurface.FootstepSounds[n] = curSurface.FootstepSounds[0];
        curSurface.FootstepSounds[0] = clip;
        
        _lastStepSurface = curSurface;
    }
}
