﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacter : MonoBehaviour
{
    public BaseController Controller
    {
        get
        {
            if (!_controller)
            {
                _controller = GetComponent<BaseController>();
            }

            return _controller;
        }
    }

    private BaseController _controller;

    private void Awake()
    {
        PostAwake();
    }

    protected virtual void PostAwake()
    {
       
    }

    public bool IsGrabSomething()
    {
        return false;
    }
}
