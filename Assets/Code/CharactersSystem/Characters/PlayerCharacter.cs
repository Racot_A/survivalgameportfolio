﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : BaseCharacter
{
    [Header("[INPUT]")]
    public InputMaster Controls;


    protected override void PostAwake()
    {
        base.PostAwake();
        Controls.Enable();
        Debug.LogError("[PLAYER] Controls Enabled");
    }
}
