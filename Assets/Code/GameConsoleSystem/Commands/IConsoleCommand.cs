﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConsoleCommand
{
    string ID { get; }
    string InputBody { get; }
    bool CanExecute(string body);
    void Execute(string body);
}
    