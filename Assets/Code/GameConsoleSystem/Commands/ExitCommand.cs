﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitCommand : IConsoleCommand
{
    public string ID => "exit";
    public string InputBody => _inputBody;

    private string _inputBody;

    public bool CanExecute(string body)
    {
        var bodyText = body.ToLower();
        return bodyText == (ID.ToLower());
    }

    public void Execute(string body)
    {
        if (string.IsNullOrEmpty(_inputBody))
        {
            _inputBody = body;
        }
        
        
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
//
        Debug.LogWarning($"[GAME CONSOLE SYSTEM] Command '{ID}' Executed");
    }
}