﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameConsoleWindow : MonoBehaviour
{
    public event Action<string> CommandCalled;
    
    [SerializeField] private Canvas RootCanvas;

    [SerializeField] private InputField CommandInputField;
    
    private bool IsVisible => RootCanvas.enabled;

    private float _cashedTimeScale;

    private int _prevCommandIndex = 0;

    private void Awake()
    {
        GameConsole.Initialize();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            Toogle();
        }
        else if(CommandInputField.isFocused && !string.IsNullOrEmpty(CommandInputField.text) && Input.GetKey(KeyCode.Return))
        {
            OnTextEntered(CommandInputField.text);
            CommandInputField.text = "";

            _prevCommandIndex = GameConsole.ExecutedCommands.Count;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            _prevCommandIndex--;
            if (_prevCommandIndex < 0)
            {
                _prevCommandIndex = GameConsole.ExecutedCommands.Count > 0 ? GameConsole.ExecutedCommands.Count - 1 : -1;
            }
            var tCommand = _prevCommandIndex >= 0 && GameConsole.ExecutedCommands.Count > _prevCommandIndex
                ? GameConsole.ExecutedCommands[_prevCommandIndex]
                : null;

            if (tCommand != null)
            {
                CommandInputField.text = tCommand.InputBody;
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            _prevCommandIndex++;
            
            if (_prevCommandIndex >= GameConsole.ExecutedCommands.Count)
            {
                _prevCommandIndex = 0;
            }
            
            var tCommand =  _prevCommandIndex >= 0 && GameConsole.ExecutedCommands.Count > _prevCommandIndex
                ? GameConsole.ExecutedCommands[_prevCommandIndex]
                : null;

            if (tCommand != null)
            {
                CommandInputField.text = tCommand.InputBody;
            }
        }
    }
    
    private void Show()
    {
        CommandInputField.ActivateInputField();
        CommandInputField.Select();

        RootCanvas.enabled = true;

        _cashedTimeScale = Time.timeScale;
        Time.timeScale = 0;
    }

    private void OnTextEntered(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            return;
        }
        CommandCalled?.Invoke(text);

        _prevCommandIndex = 0;
        
        CommandInputField.DeactivateInputField();
        CommandInputField.ActivateInputField();
        CommandInputField.Select();
    }

    private void Hide()
    {
        if (!RootCanvas)
        {
            return;
        }
        RootCanvas.enabled = false;
        
        CommandInputField.text = String.Empty;
        CommandInputField.DeactivateInputField();
        Time.timeScale = _cashedTimeScale;

    }


    public void Toogle()
    {
        if (IsVisible)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }
}
