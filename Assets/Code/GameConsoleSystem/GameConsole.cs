﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConsole
{
    public static List<IConsoleCommand> ExecutedCommands => _executedCommands;
    
    private static List<IConsoleCommand> _consoleCommands = new List<IConsoleCommand>();
    private static UIGameConsoleWindow _consoleWindow;
    private static List<IConsoleCommand> _executedCommands = new List<IConsoleCommand>();

    public static void Initialize()
    {
        _consoleWindow = GameObject.FindObjectOfType<UIGameConsoleWindow>();
        _consoleWindow.CommandCalled += ConsoleWindowOnCommandCalled;
        
        InitCommands();
        
        Debug.LogWarning($"[GAME CONSOLE SYSTEM] GameConsole Initialized");
    }
    
    private static void InitCommands()
    {
        _consoleCommands.Add(new ExitCommand());
        _consoleCommands.Add(new GetItemCommand());
    }
    
    private static void ConsoleWindowOnCommandCalled(string command)
    {
        ExecuteCommand(command);
    }

    public static void ExecuteCommand(string body)
    {
        foreach (var consoleCommand in _consoleCommands)
        {
            if (consoleCommand.CanExecute(body))
            {
                consoleCommand.Execute(body);
                _executedCommands.Add(consoleCommand);
            }
        }
    }


    #region Logging

    public static void Log(string system, string text, Object pinkTarget = null)
    {
        Debug.Log($"[{system}] {text}", pinkTarget);
    }
    public static void LogWarning(string system, string text, Object pinkTarget = null)
    {
        Debug.LogWarning($"[{system}] {text}", pinkTarget);
    }
    public static void LogError(string system, string text, Object pinkTarget = null)
    {
        Debug.LogError($"[{system}] {text}", pinkTarget);
    }

    #endregion
}
