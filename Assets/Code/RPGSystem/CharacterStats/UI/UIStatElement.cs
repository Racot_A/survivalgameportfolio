﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIStatElement : BaseGeneratedUIElement<BaseCharacterStat>
{
    public Image IconImage;
    public Image FillImage;
    public TextMeshProUGUI AmountText;

    public override void Init(BaseCharacterStat root)
    {
        base.Init(root);
        
        IconImage.sprite = Root.Info.Icon;
        IconImage.color = Root.Info.IconColor;
        
        root.Changed += UpdateContent;
    }

    public override void UpdateContent()
    {
        Fill();
    }

    private void Fill()
    {
        AmountText.SetText(Root.Value.ToString("F0"));
        FillImage.fillAmount = Root.Value * 0.01f;
        //Debug.LogError($"Root.Value:{Root.Value} FillImage.fillAmount:{FillImage.fillAmount}");
    }
}
