﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStatsPanel : UIGeneratedElementsPanel<BaseCharacterStat, UIStatElement>
{
    private BaseCharacter _owner;
    public void Show(BaseCharacter owner)
    {
        base.Show();

        _owner = owner;
        GenerateElements(owner.GetComponent<CharacterStats>().AvailableStats);
    }
    

    public override void Hide()
    {
        base.Hide();
        
        ClearElements();
    }
}
