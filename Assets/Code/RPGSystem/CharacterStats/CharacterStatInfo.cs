﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterStatInfo
{
    public float DefaultAmount = 100;
    [Header("[UI]")]
    public Sprite Icon;
    public Color IconColor = Color.white;
}
