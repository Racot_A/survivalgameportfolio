﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseCharacterStat : ScriptableObject, ICharacterStat
{
    public event Action Changed;
    public event Action ZeroValue;
    
    public string ID;
    public CharacterStatInfo Info;
    public BaseCharacter Owner => _owner;
   
    public float Value => _value;

    private BaseCharacter _owner;
    private float _value;

    public void Init(BaseCharacter character)
    {
        _owner = character;
        _value = Info.DefaultAmount;
    }
    
    public void AddValue(float amount)
    {
        _value += amount;
        if (_value > 100)
        {
            _value = 100;
        }
        OnValueChanged();
    }
    public void RemoveValue(float amount)
    {
        _value -= amount;
        if (_value < 0)
        {
            _value = 0;
        }
        OnValueChanged();
    }

    public virtual void OnValueChanged()
    {
        Changed?.Invoke();
    }

    public virtual void Begin()
    {
        
    }

    public virtual bool CanTick()
    {
        return true;
    }
    
    public virtual void Tick()
    {
      
    }
}
