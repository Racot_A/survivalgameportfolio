﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/RPG System/Stats/Health Stat")]
public class HealthStat : BaseCharacterStat
{
    public float AutoRemoveValueSpeed = 0.5f;
    public override void Tick()
    {
        base.Tick();
        
        RemoveValue(Time.deltaTime * AutoRemoveValueSpeed);
    }

    public override bool CanTick()
    {
        return base.CanTick() && Value > 0;
    }

    public override void OnValueChanged()
    {
        base.OnValueChanged();

        if (Value <= 0)
        {
            
        }
    }
}
