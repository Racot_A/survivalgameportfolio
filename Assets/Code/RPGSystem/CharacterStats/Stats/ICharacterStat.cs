﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacterStat
{   
    float Value { get; }

    void Init(BaseCharacter character);
    
    void AddValue(float amount);

    void Begin();
    void Tick();
}
