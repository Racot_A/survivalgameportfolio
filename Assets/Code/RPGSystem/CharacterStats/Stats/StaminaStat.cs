﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/RPG System/Stats/Stamina Stat")]
public class StaminaStat : BaseCharacterStat
{
    public float UseValueSpeed = 5;
    public override void Tick()
    {
        base.Tick();

        if (Owner.Controller.IsRunning())
        {
            RemoveValue(UseValueSpeed * Time.deltaTime);
        }
        else
        {
            AddValue(UseValueSpeed * Time.deltaTime);
        }
    }
}
