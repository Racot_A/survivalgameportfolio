﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [BundleAssetReference]
    public string[] DefaultStats;
    
    public List<BaseCharacterStat> AvailableStats => _stats;

    private BaseCharacter _character;
    private List<BaseCharacterStat> _stats = new List<BaseCharacterStat>();

    private void Awake()
    {
        _character = GetComponent<BaseCharacter>();

        LoadStats();
    }

    void Start()
    {
        BeginStats();
    }

    private void LoadStats()
    {
        foreach (var availableStat in DefaultStats)
        {
            var stat = AssetBundlesManager.Instance.Get<BaseCharacterStat>(availableStat);
            _stats.Add(stat);
        }
    }

    void LateUpdate()
    {
        TickStats();
    }

    private void BeginStats()
    {
        for (int i = 0; i < _stats.Count; i++)
        {
            _stats[i].Init(_character);
          
            _stats?[i].Begin();
        }
    }
    private void TickStats()
    {
        for (int i = 0; i < _stats.Count; i++)
        {
            if (_stats[i].CanTick())
            {
                _stats[i].Tick();
            }
        }
    }

    public T GetStat<T>() where T : BaseCharacterStat
    {
        return _stats.Find(x => x is T) as T;
    }
}
