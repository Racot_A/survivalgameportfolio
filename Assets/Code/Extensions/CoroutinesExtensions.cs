﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutinesExtensions
{
    public static void Stop(this Coroutine coroutine, MonoBehaviour context)
    {
        if (coroutine != null)
        {
            context.StopCoroutine(coroutine);
        }
    }
}
