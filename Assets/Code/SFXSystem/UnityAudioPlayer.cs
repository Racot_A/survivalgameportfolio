﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public static class UnityAudioPlayer
{
    public enum AudioStopMode
    {
       IMMEDIATE,
       FADEOUT
    }
    
    private static List<AudioClip> _allClips = new List<AudioClip>();
    private static Dictionary<string, AudioSource> _playedAudios = new Dictionary<string, AudioSource>();
    private static bool _isInitialized;
    
    /*[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Bootstrap()
    {
        
    }
    */

    public static void Initialize()
    {
        if (_isInitialized)
        {
            return;
        }
        LoadSettings();
        LoadClips();

        _isInitialized = true;
    }

    public static void PlayOneShoot(string sfxName, AudioSource source)
    {
        var clip = GetClip(sfxName);
        if (!clip)
        {
            Debug.LogError($"[SFX System] Not Find Clip:{sfxName}");
            return;
        }
        source.loop = false;
        source.PlayOneShot(clip);
    }
    public static void PlayLoop(string sfxName, AudioSource source)
    {
        var clip = GetClip(sfxName);
        if (!clip)
        {
            Debug.LogError($"[SFX System] Not Find Clip:{sfxName}");
            return;
        }

        SwitchSourceAudio(source, clip);
        source.Play();
        
        _playedAudios.Add(sfxName, source);
    }

    public static void SwitchSourceAudio(AudioSource source, AudioClip clip)
    {
        source.Stop();
        source.clip = clip;
    }
  
    public static void StopLoopSound(string sfxName, AudioStopMode stopMode = AudioStopMode.IMMEDIATE)
    {
        if (_playedAudios.ContainsKey(sfxName))
        {
            if (stopMode == AudioStopMode.IMMEDIATE)
            {
                _playedAudios[sfxName].Stop();
            }
            else
            {
                //TODO: Implement fade out
            }

            _playedAudios.Remove(sfxName);
        }
    }

   
    public static bool IsPlaying(string sfxName)
    {
        if (_playedAudios.ContainsKey(sfxName))
        {
            return _playedAudios[sfxName].isPlaying;
        }

        return false;
    }

    public static bool IsLoopAudio(string sfxName)
    {
        if (_playedAudios.ContainsKey(sfxName))
        {
            return _playedAudios[sfxName].loop;
        }

        return false;
    }

    public static bool Is3D(string sfxName)
    {
        if (_playedAudios.ContainsKey(sfxName))
        {
            return _playedAudios[sfxName].spatialBlend >= 0.5;
        }

        return false;
    }

    public static float GetSoundLength(string sfx)
    {
        var clip = GetClip(sfx);
        return clip ? clip.length : 0;
    }

    private static void LoadSettings()
    {
        Debug.LogWarning($"[SFX System] Settings Loaded");
    }
    private static void LoadClips()
    {
        _allClips.AddRange(AssetBundlesManager.Instance.GetAll<AudioClip>());
        Debug.LogWarning($"[SFX System] Loaded Clips:{_allClips.Count}");
    }

    public static AudioClip GetClip(string sfxName)
    {
        return _allClips.Find(x => x.name.Equals(sfxName));
    }
}
