﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEmitter : MonoBehaviour
{
    [BundleAssetReference]
    public string SfxBundleRef;

    public bool PlayAtStart = false;

    private AudioSource _source;
    
    private IEnumerator Start()
    {
        _source = GetComponent<AudioSource>();
        
        yield return new WaitForSeconds(0.5f);
        if (PlayAtStart)
        {
            Play();
        }
    }

    public void Play()
    {
        UnityAudioPlayer.PlayLoop(SfxBundleRef, _source);
    }
}
