﻿using System.Collections;
using System.Collections.Generic;
using AndrewNovak.InteractiveSystem;
using UnityEngine;

public class Door : MonoBehaviour, IOpenable, IToogled
{
    public Transform RotationTarget;
    public float RotationY = 90;
    public float RotationTime = 2;
    public bool InAnimation;

    public AnimationCurve OpenCurve;
    public AnimationCurve CloseCurve;
    
    [Header("[SFX]")]
    [SerializeField, BundleAssetReference]
    private string OpenSound;
    [SerializeField, BundleAssetReference]
    private string CloseSound;

    public bool IsOpened => _opened;
   
    private bool _opened;

    private Coroutine _rotationCor;
    
    private AudioSource _source;
    
    // Start is called before the first frame update
    void Start()
    {
        _source = GetComponentInChildren<AudioSource>();
        
        var interactive = GetComponent<Interactive>();
        interactive.Interact += InteractiveOnInteract;
    }
    
    private void InteractiveOnInteract(BaseCharacter character)
    {
        Toogle();
    }

    public void Open()
    {
        _opened = true;
        
        _rotationCor.Stop(this);
        _rotationCor = StartCoroutine(RotateCor(RotationY, OpenCurve));
        
        UnityAudioPlayer.PlayOneShoot(OpenSound, _source);
    }

    public void Close()
    {
        _opened = false;
        
        _rotationCor.Stop(this);
        _rotationCor = StartCoroutine(RotateCor(0, CloseCurve));
        
        UnityAudioPlayer.PlayOneShoot(CloseSound, _source);
    }
    
    public void Toogle()
    {
        if (IsOpened)
        {
            Close();
        }
        else
        {
            Open();
        }
    }

    private IEnumerator RotateCor(float targetYRot, AnimationCurve curve)
    {
        var i = 0.0f;
        var rate = 1f / RotationTime;
        
        var curRot = RotationTarget.localEulerAngles;
        var targetRot = new Vector3(curRot.x, targetYRot, curRot.z);
        
        while (i < 1)
        {
            i += Time.deltaTime * rate;
            curRot = Vector3.Lerp(curRot, targetRot, curve.Evaluate(i));
            RotationTarget.localEulerAngles = curRot;
            yield return null;
        }
    }

}
