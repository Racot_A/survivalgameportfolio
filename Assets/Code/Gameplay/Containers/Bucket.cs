﻿using System.Collections;
using System.Collections.Generic;
using AndrewNovak.InteractiveSystem;
using UnityEngine;

public class Bucket : MonoBehaviour, IContainer
{
    public float Mass => GetComponent<Rigidbody>() ? GetComponent<Rigidbody>().mass : 0f;
    
    // Start is called before the first frame update
    void Start()
    {
        var interactive = GetComponent<Interactive>();
        interactive.Interact += InteractiveOnInteract;
    }
    
    private void InteractiveOnInteract(BaseCharacter character)
    {
        var grabber = character.GetComponent<PlayerGrabber>();
        var interactive = GetComponent<Interactive>();
        grabber.Grab(interactive);
    }

    
}

