﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContainer
{
    float Mass { get; }
}
