﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenesConstants
{
    public const string MENU_SCENE_NAME = "Menu_Scene";
    public const string SURVIVAL_SCENE_NAME = "Survival_Scene";
}
