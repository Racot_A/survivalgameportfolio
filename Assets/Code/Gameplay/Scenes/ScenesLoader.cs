﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesLoader : MonoBehaviour
{

    public static ScenesLoader Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameObject("ScenesLoader").AddComponent<ScenesLoader>();
                _instance.Initialize();
            }

            return _instance;
        }
    }

    private static ScenesLoader _instance;

    
    public void Initialize()
    {
        GameObject.DontDestroyOnLoad(this.gameObject);
    }

    public void LoadScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
    {
        SceneManager.LoadScene(sceneName, mode);
    }
    
    public void LoadSceneAsync(string sceneName, LoadSceneMode mode = LoadSceneMode.Single,  Action complete = null)
    {
        var operation = SceneManager.LoadSceneAsync(sceneName, mode);
        operation.completed += asyncOperation => complete?.Invoke();
    }

    public void UnLoadSceneAsync(string sceneName, Action loadingComplete = null)
    {
        var operation = SceneManager.UnloadSceneAsync(sceneName);
        operation.completed += asyncOperation => loadingComplete?.Invoke();
    }
}
