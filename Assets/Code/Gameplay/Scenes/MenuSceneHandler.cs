﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneHandler : MonoBehaviour
{
    public UIMenuWindow MenuWindow;
    private void Start()
    {
        var optionsPanel = MenuWindow.GetPanel<UIMenuOptionsPanel>();
        if (optionsPanel)
        {
            optionsPanel.NewGameClicked += OptionsPanelOnNewGameClicked;
            optionsPanel.ExitClicked += OptionsPanelOnExitClicked;
        }
    }

    private void OptionsPanelOnNewGameClicked()
    {
        ScenesLoader.Instance.LoadSceneAsync(ScenesConstants.SURVIVAL_SCENE_NAME, LoadSceneMode.Additive, OnSurvivalSceneLoaded);
    }

    private void OnSurvivalSceneLoaded()
    {
        //Unload Menu Scene
        ScenesLoader.Instance.UnLoadSceneAsync(ScenesConstants.MENU_SCENE_NAME);
    }

    private void OptionsPanelOnExitClicked()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
}

