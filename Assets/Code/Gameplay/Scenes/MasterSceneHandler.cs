﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MasterSceneHandler : MonoBehaviour
{

    [SerializeField] private AssetBundlesManager BundlesManagerPref;
    
    private void Awake()
    {
        if (AssetBundlesManager.Instance == null)
        {
            Instantiate(BundlesManagerPref);
        }
    }

    void Start()
    {
        UnityAudioPlayer.Initialize();
        
        ScenesLoader.Instance.LoadSceneAsync(ScenesConstants.MENU_SCENE_NAME, LoadSceneMode.Additive);
    }

}
