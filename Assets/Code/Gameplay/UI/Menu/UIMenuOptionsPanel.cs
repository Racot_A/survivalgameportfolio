﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenuOptionsPanel : BaseUIPanel
{
    public event Action NewGameClicked;
    public event Action ExitClicked;
    
    [SerializeField] private Button NewGameButton; 
    [SerializeField] private Button ExitButton; 
    
    public override void Show()
    {
        base.Show();
        
        gameObject.SetActive(true);

        NewGameButton.onClick.AddListener(StartNewGame);
        ExitButton.onClick.AddListener(ExitFromGame);
    }

    
    public override void Hide()
    {
        base.Hide();
        
        gameObject.SetActive(false);
    }

    
    private void StartNewGame()
    {
        NewGameClicked?.Invoke();
    }
    
    private void ExitFromGame()
    {
        ExitClicked?.Invoke();
    }
}
