﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMenuWindow : BaseUIWindow
{
    public override void Show()
    {
        base.Show();
        
        gameObject.SetActive(true);

    }

    public override void Hide()
    {
        base.Hide();
        
        gameObject.SetActive(false);
    }
}
