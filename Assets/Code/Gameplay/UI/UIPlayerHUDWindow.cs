﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerHUDWindow : BaseUIWindow
{

    protected override void PostStart()
    {
        Show();
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1);
        Show();
    }

    public override void Show()
    {
        base.Show();
        var statsPanel = GetPanel<UIStatsPanel>();
        var player = FindObjectOfType<PlayerCharacter>();
        statsPanel.Show(player);
        
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        base.Hide();
        
        gameObject.SetActive(false);
    }
}
