﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

public class DatabaseAssetsLoader : IAssetsLoader
{

#if UNITY_EDITOR
    private readonly List<string> _assets = new List<string>();
#endif
    public DatabaseAssetsLoader()
    {
#if UNITY_EDITOR
        Debug.LogError("[Asset Bundles] Init DatabaseAssetsLoader");
        
        _assets.Clear();
        foreach (var assetBundleName in UnityEditor.AssetDatabase.GetAllAssetBundleNames())
        {
            var bundleAssetPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundle(assetBundleName);
            _assets.AddRange(bundleAssetPaths);
        }
#endif
    }

    public T Load<T>(string fileName) where T : Object
    {
#if UNITY_EDITOR
        var assetPath = _assets.Find(item =>
        {
            var assetName = Path.GetFileNameWithoutExtension(item);

            return assetName.ToLower().Equals(fileName.ToLower());
        });

        return !string.IsNullOrEmpty(assetPath) ? UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath) : default(T);
#else
        return default(T);
#endif

        
    }

    public T[] LoadAll<T>() where T : Object
    {
#if UNITY_EDITOR
        {
            var loadedAssets = new List<T>();
            foreach (var assetPath in _assets)
            {
                var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath);
                if (asset != null)
                {
                    loadedAssets.Add(asset);
                }
            }

            return loadedAssets.ToArray();
        }
#else
        return null;
#endif

        
    }

    public void LoadAsync<T>(string fileName, Action<T> onLoaded) where T : Object
    {
        if (onLoaded != null)
        {
            onLoaded(Load<T>(fileName));
        }
    }

    public void LoadAsyncAll<T>(Action<T[]> onLoaded) where T : Object
    {
        if (onLoaded != null)
        {
            onLoaded(LoadAll<T>());
        }
    }

#if UNITY_EDITOR
    public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
    {
        List<T> assets = new List<T>();
        //Debug.LogError(string.Format("t:{0}", typeof(T)));
        string[] guids = UnityEditor.AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                assets.Add(asset);
            }
        }

        return assets;
    }
#endif
}