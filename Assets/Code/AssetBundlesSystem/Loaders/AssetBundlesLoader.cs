﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

public class AssetBundlesLoader : IAssetsLoader
{
    private AssetBundlesSettings _settings;

    private event Action _onDataLoaded;
    private List<AssetBundle> _bundles = new List<AssetBundle>();
    private AssetBundleManifest _bundlesManifest;

    #region Assets

    public T Load<T>(string fileName) where T : Object
    {
        var tPath = fileName.ToLower();

        foreach (var bundle in GetLoadedBundles())
        {
            var names = bundle.GetAllAssetNames();

            foreach (var name in names)
            {
                var tName = Path.GetFileNameWithoutExtension(name);

                //Debug.LogError(string.Format("Bundle:{0} Name:{1} TName:{2}", bundle, name, tName));

                if (tName == tPath)
                {
                    return bundle.LoadAsset<T>(tPath);
                }
            }
        }

        Debug.LogError(string.Format("Not find file:{0}", tPath));
        return null;
    }

    public T[] LoadAll<T>() where T : Object
    {
        var loadedAssets = new List<T>();
        foreach (var bundle in GetLoadedBundles())
        {
            loadedAssets.AddRange(bundle.LoadAllAssets<T>());
        }

        return loadedAssets.ToArray();
    }

    public void LoadAsync<T>(string fileName, Action<T> onLoaded) where T : Object
    {
        foreach (var bundle in GetLoadedBundles())
        {
            var names = bundle.GetAllAssetNames();
            if (Array.Exists(names, x => x == fileName))
            {
                var asyncRequest = bundle.LoadAssetAsync<T>(fileName);
                asyncRequest.completed += operation =>
                {
                    if (onLoaded != null)
                    {
                        onLoaded.Invoke(asyncRequest.asset as T);
                    }
                };
            }
        }
    }

    public void LoadAsyncAll<T>(Action<T[]> onLoaded) where T : Object
    {
        foreach (var bundle in GetLoadedBundles())
        {
            var asyncRequest = bundle.LoadAllAssetsAsync<T>();
            asyncRequest.completed += operation =>
            {
                List<T> loadedAssets = new List<T>();

                foreach (var asset in asyncRequest.allAssets)
                {
                    loadedAssets.Add(asset as T);
                }

                if (onLoaded != null)
                {
                    onLoaded.Invoke(loadedAssets.ToArray());
                }
            };
        }
    }

    #endregion

    #region Bundles

    public bool IsBundlePresentInManifest(string checkedBundleName)
    {
        var present = Array.Exists(_bundlesManifest.GetAllAssetBundles(), element => element == checkedBundleName);
        //Debug.LogError("Bundle:" + checkedBundleName + " Pres:" + present);
        return present;
    }

    public AssetBundlesLoader(AssetBundlesSettings settings, Action onDataLoaded)
    {
        Debug.LogError("[Asset Bundles] Init AssetBundlesLoader");

        _settings = settings;
        _onDataLoaded = onDataLoaded;
        _bundles.Clear();

        LoadBundlesManifest("AssetBundles", OnManifestLoaded);
    }

    private void OnManifestLoaded(AssetBundleManifest manifest)
    {
        //Background bundles
        StartLoadingBackgroundBundles();
        //Required Bundles
        StartLoadingRequiredBundles();
    }

    private void StartLoadingBackgroundBundles()
    {
        var backgroundBundles = _settings.PreloadBackgroundBundles;
        foreach (var bundle in backgroundBundles)
        {
            if (IsBundlePresentInManifest(bundle))
            {
                LoadBundleAsync(bundle, OnBundleLoaded);
            }
        }
    }

    private void StartLoadingRequiredBundles()
    {
        var requiredBundles = _settings.PreloadRequiredBundles;
        foreach (var bundle in requiredBundles)
        {
            if (IsBundlePresentInManifest(bundle))
            {
                LoadBundle(bundle, OnBundleLoaded);
            }
        }
    }

    public List<AssetBundle> GetLoadedBundles()
    {
        return _bundles;
    }

    private void UnloadBundle(string bundleName)
    {
        var loadedBundle = _bundles.Find(x => x.name.Equals(bundleName));
        if (!loadedBundle)
        {
            Debug.LogWarning(string.Format("Bundle: {0} is not loaded", bundleName));
            return;
        }

        loadedBundle.Unload(false);
        _bundles.Remove(loadedBundle);
    }

    private void LoadBundlesManifest(string manifestName, Action<AssetBundleManifest> onLoaded)
    {
        //Debug.LogError("Load Manifest:" + manifestName);
        LoadBundle(manifestName, bundle =>
        {
            _bundlesManifest = bundle.LoadAsset<AssetBundleManifest>("assetbundlemanifest");
            Debug.LogWarning("[Asset Bundles] Loaded Bundles Manifest");
            if (onLoaded != null)
            {
                onLoaded.Invoke(_bundlesManifest);
            }
        });
    }

    private void LoadBundle(string assetBundleName, Action<AssetBundle> onLoaded)
    {
        Debug.Log("[Asset Bundles] LoadBundle:" + assetBundleName);
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles");
        filePath = System.IO.Path.Combine(filePath, assetBundleName);
        if (onLoaded != null)
        {
            onLoaded.Invoke(AssetBundle.LoadFromFile(filePath));
        }
    }

    private void LoadBundleAsync(string assetBundleName, Action<AssetBundle> onLoaded)
    {
        float startLoadTime = Time.realtimeSinceStartup;
        float endLoadTime = 0f;

        //Debug.Log("LoadBundleAsync:" + assetBundleName);
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles");
        filePath = System.IO.Path.Combine(filePath, assetBundleName);
        var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(filePath);
        //Set Loading priority
        assetBundleCreateRequest.priority = (int) _settings.BundlesLoadPriority;

        assetBundleCreateRequest.completed += operation =>
        {
            endLoadTime = Time.realtimeSinceStartup;
            Debug.LogFormat("[Asset Bundles] Loading Bundle: {0} Time:{1}", assetBundleName,
                endLoadTime - startLoadTime);
            AssetBundle assetBundle = assetBundleCreateRequest.assetBundle;
            if (onLoaded != null)
            {
                onLoaded.Invoke(assetBundle);
            }
        };
    }

    private void OnBundleLoaded(AssetBundle assetBundle)
    {
        _bundles.Add(assetBundle);
        Debug.Log(string.Format("[Asset Bundles] Bundle {0} Loaded", assetBundle.name));

        if (_bundles.Count >= _bundlesManifest.GetAllAssetBundles().Length)
        {
            Debug.LogWarning("[Asset Bundles] All Bundles Loaded");
            if (_onDataLoaded != null)
            {
                _onDataLoaded.Invoke();
            }
        }
    }

    #endregion

/*
   
#if UNITY_EDITOR    
    public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
    {

        List<T> assets = new List<T>();
        //Debug.LogError(string.Format("t:{0}", typeof(T)));
        string[] guids = UnityEditor.AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                assets.Add(asset);
            }
        }
        return assets;
    }
#endif*/
}