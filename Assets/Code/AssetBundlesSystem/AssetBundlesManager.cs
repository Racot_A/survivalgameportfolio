﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

public class AssetBundlesManager : MonoBehaviour, IBundlesManager
{
    public enum LoaderType
    {
        FromDatabase,
        FromBundles
    }
    
    public event Action AllBundlesLoaded;
    /*public event Action BackgroundBundlesLoaded;
    public event Action RequiredBundlesLoaded;*/
    
    public static AssetBundlesManager Instance;
    
    public AssetBundlesSettings BundlesSettings;
    [Header("[Editor Only]")]
    public LoaderType AssetsLoadingVariant = LoaderType.FromDatabase;
    
    private IAssetsLoader _assetsLoader;

    /*[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Bootstrap()
    {
        
    }*/
    
    private void Awake()
    {
        Instance = this;
        
        DontDestroyOnLoad(gameObject);

        InitLoaded();
    }

    private void InitLoaded()
    {
        
#if UNITY_EDITOR
        
        Debug.LogError("[Asset Bundles] Assets Loading Variant:" + AssetsLoadingVariant);

        if (AssetsLoadingVariant == LoaderType.FromBundles)
        {
            _assetsLoader = new AssetBundlesLoader(BundlesSettings, AllBundlesLoaded);
        }
        else
        {
            _assetsLoader = new DatabaseAssetsLoader();
        }
#else
			_assetsLoader = new AssetBundlesLoader(BundlesSettings, AllBundlesLoaded);
#endif
    }

    public T Get<T>(string fileName) where T : Object
    {
        var tPath = fileName.ToLower();
        return _assetsLoader.Load<T>(tPath);
    }

    public T[] GetAll<T>() where T : Object
    {
        return _assetsLoader.LoadAll<T>();
    }
 
    public void GetAsync<T>(string fileName, Action<T> onLoaded) where T : Object
    {
        var tPath = fileName.ToLower();
        _assetsLoader.LoadAsync<T>(tPath, onLoaded);
    }
    public void GetAsyncAll<T>(Action<T[]> onLoaded) where T : Object
    {
        _assetsLoader.LoadAsyncAll<T>(onLoaded);
    }
}
