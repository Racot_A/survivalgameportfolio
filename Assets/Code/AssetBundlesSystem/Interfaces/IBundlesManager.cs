﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

public interface IBundlesManager
{    
    T Get<T>(string fileName) where T : Object;
    T[] GetAll<T>() where T : Object;

    void GetAsync<T>(string fileName, Action<T> onLoaded) where T : Object;
    void GetAsyncAll<T>(Action<T[]> onLoaded) where T : Object;
}
