﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

public interface IAssetsLoader
{
    T Load<T>(string fileName) where T : Object;
    T[] LoadAll<T>() where T : Object;
    void LoadAsync<T>(string fileName, Action<T> onLoaded) where T : Object;
    void LoadAsyncAll<T>(Action<T[]> onLoaded) where T : Object;
}
