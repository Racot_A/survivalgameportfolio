﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class BundlesEditorWindow : EditorWindow
{
    private BuildTarget _buildTarget = BuildTarget.NoTarget;
    private BuildAssetBundleOptions _buildOptions = BuildAssetBundleOptions.ChunkBasedCompression;
    //private string _bundlesBuildFolderName = "AssetBundles";
    private string _bundlesBuildPath;
    
    [MenuItem("Tools/AssetBundles/Bundles Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        BundlesEditorWindow window = (BundlesEditorWindow)EditorWindow.GetWindow(typeof(BundlesEditorWindow));
        window.titleContent.text = "Bundles Window";
        window.Show();

    }


    private void OnEnable()
    {
        _bundlesBuildPath = Path.Combine(Application.streamingAssetsPath, "AssetBundles");
        _bundlesBuildPath = _bundlesBuildPath.Replace(@"\","/");

        _buildTarget = EditorUserBuildSettings.activeBuildTarget;
    }

    private bool toogleGroup;

    private bool PC;
    private bool Android;
    
    void OnGUI()
    {
        GUILayout.Label("Build Settings", EditorStyles.boldLabel);
        
        
        _bundlesBuildPath = EditorGUILayout.TextField("Build Path", _bundlesBuildPath);
        
        if (GUILayout.Button("Select Path"))
        {
            var selectedPath = EditorUtility.OpenFolderPanel("", _bundlesBuildPath, "");
            selectedPath = selectedPath.Replace(@"\","/");
            Debug.LogError("Path:" + selectedPath);
            _bundlesBuildPath = selectedPath;
        }

        _buildTarget = (BuildTarget)EditorGUILayout.EnumPopup("Build Target", _buildTarget);
        _buildOptions = (BuildAssetBundleOptions)EditorGUILayout.EnumPopup("Build Options", _buildOptions);

        if (GUILayout.Button("Build"))
        {
            BuildBundles();
        }
    }


    private void BuildBundles()
    {
        if (_buildTarget == BuildTarget.NoTarget || _buildOptions == BuildAssetBundleOptions.None || string.IsNullOrEmpty(_bundlesBuildPath))
        {
            return;
        }
        
        BuildPipeline.BuildAssetBundles(_bundlesBuildPath, _buildOptions, _buildTarget);
        AssetDatabase.Refresh();
        Debug.LogWarning("Asset Bundle Build Success");
    }
}

