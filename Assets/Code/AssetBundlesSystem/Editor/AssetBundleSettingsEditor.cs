﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(AssetBundlesSettings))]
public class AssetBundleSettingsEditor : Editor
{
    private StringsReorderableList _preloadBackgroundBundlesList;
    private StringsReorderableList _preloadRequiredBundlesList;

    public AssetBundlesSettings EditorTarget
    {
        get { return target as AssetBundlesSettings; }
    }
    
    private void OnEnable()
    {
        var allBundles = AssetDatabase.GetAllAssetBundleNames();
        _preloadBackgroundBundlesList = new StringsReorderableList(EditorTarget.PreloadBackgroundBundles, serializedObject, serializedObject.FindProperty("PreloadBackgroundBundles"), allBundles.ToArray());
        _preloadRequiredBundlesList = new StringsReorderableList(EditorTarget.PreloadRequiredBundles, serializedObject, serializedObject.FindProperty("PreloadRequiredBundles"), allBundles.ToArray());
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
            
        serializedObject.Update();

        if (GUILayout.Button("Fill All Bundles"))
        {
            EditorTarget.AllBundles.Clear();
            EditorTarget.AllBundles = new List<string>(AssetDatabase.GetAllAssetBundleNames());
        }
        
        _preloadBackgroundBundlesList.Update();
        _preloadRequiredBundlesList.Update();
        
        serializedObject.ApplyModifiedProperties();
    }
}
