﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Experimental.Build.AssetBundle;
using UnityEngine;

[CustomPropertyDrawer(typeof(BundleAssetReferenceAttribute))]
public class BundleAssetReferenceDrawer : PropertyDrawer
{    
    private List<string> _options;
    int _selectedOption = 0;

    private GUIStyle _popupStyle;

    private string[] _results;
    
    Dictionary<string, string[]> _cashedResults = new Dictionary<string, string[]>();
    
    

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var propertyType = property.propertyType;

        if (propertyType == SerializedPropertyType.String)
        {
            if (_popupStyle == null)
            {
                _popupStyle = new GUIStyle(EditorStyles.popup);
                _popupStyle.alignment = TextAnchor.MiddleCenter;
            }

            if (_options == null)
            {
                _options = new List<string> {"..."};
                _options.AddRange(AvailableOptions);
            }

            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position,
                GUIUtility.GetControlID(FocusType.Passive),
                label);

            label.text = property.stringValue;
            label.tooltip = "AssetName"; //assetReference.AssetName;

            /*int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;*/

            
            var helpBoxRect = new Rect(position.x, position.y, position.width * 0.85f - 5, position.height);

            var optionsFieldRect = new Rect(position.x + position.width * 0.85f, position.y, position.width * 0.15f,
                position.height);

            /*var rect = new Rect(position.x - position.width * 0.15f - 5, position.y, position.width * 0.15f - 5, position.height);
            GUI.Box(rect,GUIContent.none);
            GUI.Label(rect, property.stringValue);*/

            EditorGUI.ObjectField(helpBoxRect, string.IsNullOrEmpty(property.stringValue) ? null : GetAssetByName(property.stringValue) , typeof(UnityEngine.Object), false);

            EditorGUI.BeginChangeCheck();

            _selectedOption = EditorGUI.Popup(optionsFieldRect, _selectedOption, _options.ToArray(), _popupStyle);

            if (EditorGUI.EndChangeCheck())
            {
                if (_selectedOption > 0)
                {
                    var tName = _options[_selectedOption].Remove(0,
                        _options[_selectedOption].LastIndexOf("/", StringComparison.Ordinal) + 1);

                    property.stringValue = GetAssetByName(tName).name;
                    _selectedOption = 0;
                    
                    //Debug.LogError("property changed:" + property.displayName);
                }
            }
        }
        else
        {
            EditorGUI.HelpBox(position,
                string.Format("{0} Only Support string type", typeof(BundleAssetReferenceAttribute).Name),
                MessageType.Error);
            Debug.LogError("Wrong Property Type of " + typeof(BundleAssetReferenceAttribute).Name,
                property.serializedObject.targetObject);
        }
    }

    private UnityEngine.Object GetAssetByName(string objectName)
    {
        if (_cashedResults.ContainsKey(objectName))
        {
            _results = _cashedResults[objectName];
        }
        else
        {
            _cashedResults.Add(objectName, AssetDatabase.FindAssets(objectName));
            _results = _cashedResults[objectName];
        }

        if (_results.Length <= 0)
        {
            return null;
        }

        foreach (var result in _results)
        {
            var path = AssetDatabase.GUIDToAssetPath(result);
            var name = Path.GetFileNameWithoutExtension(path);
            if (!string.IsNullOrEmpty(name) && name.Equals(objectName))
            {
                var bundleName = AssetDatabase.GetImplicitAssetBundleName(path);
                if (!string.IsNullOrEmpty(bundleName))
                {
                    var ob = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                    return ob;
                }
            }
        }

        return null;
    }

    public List<string> AvailableOptions
    {
        get
        {
            var options = new List<string>();

            var bundlesNames = AssetDatabase.GetAllAssetBundleNames();
            foreach (var bundlesName in bundlesNames)
            {
                var bundleAssetsPaths = AssetDatabase.GetAssetPathsFromAssetBundle(bundlesName);
                foreach (var bundleAssetsPath in bundleAssetsPaths)
                {
                    var assetName = Path.GetFileNameWithoutExtension(bundleAssetsPath);
                    options.Add(string.Format("{0}/{1}", bundlesName, assetName));
                }
            }

            return options;
        }
    }
}