﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AssetBundleTools : Editor
{
    
    /*public class CreateAssetBundles
    {
        [MenuItem("Tools/AssetBundles/Build Asset Bundles")]
        static void BuildAssetBundles()
        {
            //var useBundleSystem = UnityEditor.EditorPrefs.GetBool("UseBundleSystem", false);
            
            string folderName = "AssetBundles";
            string filePath = Path.Combine(Application.streamingAssetsPath, folderName);
            Debug.LogError("BuildAssetBundles to path:" + filePath);

            //DEL: All Old Files
            System.IO.DirectoryInfo di = new DirectoryInfo(filePath);
            
            if (di.Exists)
            {
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete(); 
                }
            }
           
            //Build for Windows platform
            BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows64);

            //Uncomment to build for other platforms
            //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.iOS);
            //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.Android);
            //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.WebGL);
            //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);

            //Refresh the Project folder
            AssetDatabase.Refresh();
        }
    }*/
    
    public static class CheckmarkMenuItem {
  
        private const string MENU_NAME = "Tools/AssetBundles/Use Bundle In Editor";
  
        private static bool enabled_;
        /// Called on load thanks to the InitializeOnLoad attribute
        static CheckmarkMenuItem() {
            CheckmarkMenuItem.enabled_ = EditorPrefs.GetBool(CheckmarkMenuItem.MENU_NAME, false);
  
            /// Delaying until first editor tick so that the menu
            /// will be populated before setting check state, and
            /// re-apply correct action
            EditorApplication.delayCall += () => {
                PerformAction(CheckmarkMenuItem.enabled_);
            };
        }
  
        [MenuItem(CheckmarkMenuItem.MENU_NAME)]
        private static void ToggleAction() {
  
            /// Toggling action
            PerformAction( !CheckmarkMenuItem.enabled_);
        }
  
        public static void PerformAction(bool enabled) {
  
            /// Set checkmark on menu item
            Menu.SetChecked(CheckmarkMenuItem.MENU_NAME, enabled);
            /// Saving editor state
            EditorPrefs.SetBool(CheckmarkMenuItem.MENU_NAME, enabled);
  
            CheckmarkMenuItem.enabled_ = enabled;
  
            /// Perform your logic here...
        }
    }
}
