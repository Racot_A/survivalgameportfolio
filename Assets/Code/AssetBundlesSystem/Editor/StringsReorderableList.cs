﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class StringsReorderableList
{
    private SerializedObject _serializedObject;
    private ReorderableList _reorderableList;
    private string _tittleName;

    private string[] _aviableAddElements;

    private List<string> _targetList;


    public StringsReorderableList(List<string> targetList, SerializedObject serializedObject, SerializedProperty property, string[] aviableAddElements)
    {
        _targetList = targetList;
        _serializedObject = serializedObject;
        _aviableAddElements = aviableAddElements;

        _reorderableList = new ReorderableList(serializedObject, property);
        _tittleName = property.name;


        _reorderableList.drawElementCallback = DrawElementCallback;
        _reorderableList.drawHeaderCallback += DrawHeaderCallback;
        _reorderableList.onAddDropdownCallback += OnAddDropdownCallback;
        _reorderableList.onRemoveCallback += OnRemoveCallback;
        _reorderableList.onCanRemoveCallback += OnCanRemoveCallback;
    }

    private bool OnCanRemoveCallback(ReorderableList list)
    {
        return _targetList.Count > 0;
    }

    private void OnRemoveCallback(ReorderableList list)
    {
        if (list.index == -1)
            return;

        EditorApplication.Beep();
        if (EditorUtility.DisplayDialog("Remove element", string.Format("Do you want remove {0}?", _targetList[list.index]), "Yes", "Cancel"))
        {
            _targetList.RemoveAt(list.index);
            EditorUtility.SetDirty(_serializedObject.targetObject);
        }
    }

    private void OnAddDropdownCallback(Rect buttonRect, ReorderableList list)
    {
        var menu = new GenericMenu();
        foreach (var addElement in _aviableAddElements)
        {
            var element = addElement;
            if (_targetList.Contains(element))
            {
                continue;
            }
            menu.AddItem(new GUIContent(element), false, () => OnAddElement(element));
        }
        menu.ShowAsContext();

    }

    private void OnAddElement(string element)
    {
        _serializedObject.Update();
        _targetList.Add(element);
        _serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(_serializedObject.targetObject);
        /*() => {
            _serializedObject.pro
            _serializedObject.Update();
            list.serializedProperty.arraySize++;
            _serializedObject.ApplyModifiedProperties();*/
    }

    public void Update()
    {
        _reorderableList.DoLayoutList();
    }

    private void DrawHeaderCallback(Rect rect)
    {
        var tittle = string.Format("{0} ({1})", _tittleName, ElementsAmount);
        EditorGUI.LabelField(rect, tittle);
    }

    private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
    {
        var element = _reorderableList.serializedProperty.GetArrayElementAtIndex(index);
        rect.y += 2;
        element.stringValue =
            EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                element.stringValue);
    }

    public int ElementsAmount
    {
        get { return _reorderableList.serializedProperty.arraySize; }
    }
}