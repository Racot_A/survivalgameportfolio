﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/AssetBundles/Asset Bundle Settings")]
public class AssetBundlesSettings : ScriptableObject
{
    public ThreadPriority BundlesLoadPriority = ThreadPriority.Low;
    [HideInInspector]
    public List<string> PreloadBackgroundBundles = new List<string>();
    [HideInInspector]
    public List<string> PreloadRequiredBundles = new List<string>();
    
    
    public List<string> AllBundles = new List<string>();
}
