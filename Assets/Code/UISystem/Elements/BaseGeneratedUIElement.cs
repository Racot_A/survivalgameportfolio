﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGeneratedUIElement <T> : MonoBehaviour
{
    public T Root { get; private set; }

    public virtual void Init(T root)
    {
        Root = root;
        
        UpdateContent();
    }
    

    public virtual void UpdateContent()
    {
        
    }
    
    public virtual void DestroyElement()
    {
        Destroy(gameObject);
    }
}
