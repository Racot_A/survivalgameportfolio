﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWindow
{
    void Show();
    void Hide();
    void Toogle();
}
