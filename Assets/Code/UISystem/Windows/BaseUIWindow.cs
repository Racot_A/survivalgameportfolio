﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUIWindow : MonoBehaviour, IPanel
{
    public enum UIWindowState
    {
        VISIBLE,
        HIDEN
    }
    
    [SerializeField] protected UIWindowState DefaultState;
    
    public bool IsVisible => _currentState == UIWindowState.VISIBLE;
    
    private UIWindowState _currentState;
    
    private void Start()
    {
        PostStart();
    }

    protected virtual void PostStart()
    {
        var panels = GetPanels(true);
        foreach (var panel in panels)
        {
            if (panel.DefaultState == BaseUIPanel.UIPanelState.HIDEN)
            {
                panel.Hide();
            }
            else
            {
                panel.Show();
            }
        }
        
        if (DefaultState == UIWindowState.VISIBLE)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    public virtual void Show()
    {
        _currentState = UIWindowState.VISIBLE;
    }

    public virtual void Hide()
    {
        _currentState = UIWindowState.HIDEN;
    }

    public virtual void Toogle()
    {
        if (IsVisible)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }
    
    public T GetPanel<T>() where T : BaseUIPanel
    {
        return gameObject.GetComponentInChildren<T>(true);
    }

    /*public T GetPanel<T>(string panelName) where T : BaseUIPanel
    {
        var panels = gameObject.GetComponentsInChildren<T>(true);
        return Array.Find(panels, x => x.PanelID.Equals(panelName));
    }*/

    public BaseUIPanel[] GetPanels(bool includeInactive)
    {
        return gameObject.GetComponentsInChildren<BaseUIPanel>(includeInactive);
    }

    public T[] GetPanels<T>(bool includeInactive) where T : BaseUIPanel
    {
        return gameObject.GetComponentsInChildren<T>(includeInactive);
    }


    /*public void SwitchToPanel(string panelName, bool ignoreLockedPanels = false)
    {
        //Debug.Log("SwitchToPanel:" + panelName);

        var panels = GetPanels(true);

        foreach (var panel in panels)
        {
            if (panel.PanelID.Equals(panelName))
            {
                if (!panel.Locked || ignoreLockedPanels)
                {
                    panel.Show();
                }
            }
            else
            {
                if (!panel.Locked || ignoreLockedPanels)
                {
                    panel.Hide();
                }
            }
        }
    }*/
    public void SwitchToPanel(BaseUIPanel targetPanel, bool ignoreLockedPanels = false)
    {
        //Debug.Log("SwitchToPanel:" + panelName);

        var panels = GetPanels(true);

        foreach (var panel in panels)
        {
            if (panel.Equals(targetPanel))
            {
                if (!panel.Locked || ignoreLockedPanels)
                {
                    panel.Show();
                }
            }
            else
            {
                if (!panel.Locked || ignoreLockedPanels)
                {
                    panel.Hide();
                }
            }
        }
    }
}
