﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGeneratedElementsPanel<T, E> : BaseUIPanel where E : BaseGeneratedUIElement<T>
{
    [Header("[UIGeneratedElementsPanel]")]
    public E UIElementPref;
    public RectTransform ElementsContainer;
    
    private List<E> _generatedElements = new List<E>();
    
    
    public void GenerateElements(List<T> dataList)
    {
        foreach (var elementData in dataList)
        {
            var instance = InstantiateNewUiElement();
            OnNewElementInstantiated(instance);
            instance.Init(elementData);
        }
    }
    
    public void ClearElements()
    {
        var needDestroyElements = new List<E>(_generatedElements);
        
        foreach (var uiElement in needDestroyElements)
        {
            OnNewElementDestroyed(uiElement);
            uiElement.DestroyElement();
        }
    }


    private E InstantiateNewUiElement()
    {
        var element = Instantiate<E>(UIElementPref, ElementsContainer);
        return element;
    }

    public virtual void OnNewElementInstantiated(E element)
    {
        _generatedElements.Add(element);
    }
    public virtual void OnNewElementDestroyed(E element)
    {
        _generatedElements.Remove(element);
    }


    public E GeUIElement(T root)
    {
        return _generatedElements.Find(x => x.Root.Equals(root));
    }
    
}
