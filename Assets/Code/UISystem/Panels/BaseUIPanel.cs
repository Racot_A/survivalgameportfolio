﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUIPanel : MonoBehaviour, IPanel
{
    public enum UIPanelState
    {
        VISIBLE,
        HIDEN
    }
    
    public UIPanelState DefaultState;
    public bool Locked;
    
    
    public bool IsVisible => _currentState == UIPanelState.VISIBLE;
    
    private UIPanelState _currentState;
    
    
    public virtual void Show()
    {
        _currentState = UIPanelState.VISIBLE;
    }

    public virtual void Hide()
    {
        _currentState = UIPanelState.HIDEN;
    }

    public virtual void Toogle()
    {
        if (_currentState == UIPanelState.VISIBLE)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }
}
