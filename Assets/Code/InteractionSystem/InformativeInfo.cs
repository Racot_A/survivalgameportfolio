﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AndrewNovak.InteractiveSystem
{
	[CreateAssetMenu(fileName = "InteractiveInfo", menuName = "Scriptable Objects/Interactive/InteractiveInfo", order = 1)]
	public class InformativeInfo : ScriptableObject
	{
		public string Name;
		public string Description;

		public Sprite Icon;

#if UNITY_EDITOR
		[ContextMenu("Rename Asset From Info Name")]
		private void RenameAssetFromName()
		{
			var clearName = Name.Replace(" ","_");
			string newName = string.Format("II_{0}", clearName);

			string result = UnityEditor.AssetDatabase.RenameAsset(UnityEditor.AssetDatabase.GetAssetPath(this), newName);
			if (string.IsNullOrEmpty(result))
			{
				UnityEditor.EditorUtility.SetDirty(this);
				UnityEditor.AssetDatabase.SaveAssets();
				UnityEditor.AssetDatabase.Refresh();
			}
			else
			{
				Debug.LogError(result);
			}
		}
#endif
	}
}
