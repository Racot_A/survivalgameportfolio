﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.Input;

//using UnityEngine.Experimental.Input;

namespace AndrewNovak.InteractiveSystem
{
	public class PlayerInteraction : MonoBehaviour
	{
		public Action<BaseCharacter> OnBeginInteract;
		public Action OnEndInteract;

		public Camera RayCastCamera;
		public LayerMask InteractionLayerMask;
		public LayerMask InteractionBlockLayerMask;
		public float MaxInteractionDistance = 200f;
		public float MoreActionsDelay = 0.5f;

		private Interactive _interactiveObject;
		private Interactive _lastInteractive;

		private Ray _ray;
		private RaycastHit _hitInfo;

		//private WaitTimeCoroutine _beginInteractDelay;

		private BaseCharacter _owner;

		private int _interactionBlockerMask;
		
		/*private UIPlayerInveractiveActionsWindow _actionsWindow;

		private UIInteractiveActorInfoPanel _infoPanel;

		private CustomCoroutine _moreActionsCor;*/

		private float _startInteractTime;

		// Use this for initialization
		void Awake()
		{
			var player = GetComponent<PlayerCharacter>();
			//NOTE: InteractAction MUST have PRESS AND RELEASE Interaction
			player.Controls.Player.Interact.performed += InteractActionOnStarted;
			player.Controls.Player.Tablet.performed += context => {Debug.LogError("AAAA");};

			_owner = GetComponent<BaseCharacter>();

			_interactionBlockerMask = LayerMask.GetMask(new[] {"InteractionBlocker"});
		}

		private void Start()
		{
			//Init UI
			/*var hud = ServiceLocator.GetService<UIManager>().GetWindow<UIPlayerHUD>(UIConstants.UIWindows.PLAYER_HUD_WINDOW);
			_infoPanel = hud.GetPanel<UIInteractiveActorInfoPanel>();
			//Init Actions window	
			_actionsWindow = ServiceLocator.GetService<UIManager>()
				.GetWindow<UIPlayerInveractiveActionsWindow>(UIConstants.UIWindows.PLAYER_INTERACTIVE_ACTIONS_WINDOW);*/
		}

		private void InteractActionOnStarted(InputAction.CallbackContext callbackContext)
		{
			//Debug.LogError(callbackContext.interaction.GetType() + " Started");

			if (_interactiveObject != null && _interactiveObject.CanInteract(_owner, _hitInfo.distance))
			{
				BeginInteract(_owner);
				Debug.Log("[INTERACTION] BeginInteract with:" + _hitInfo.collider.name);
			}
		}


		public bool InteractiveActorInFocus()
		{
			return _interactiveObject != null;
		}
		
		public void BeginInteract(BaseCharacter character)
		{
			_interactiveObject.OnInteract(character);
			OnBeginInteract?.Invoke(character);
		}

		public bool InActionWindow()
		{
			return false; //_actionsWindow.GetCurrentState() != BaseUIWindow.UIWindowState.Hide;
		}

		void FixedUpdate()
		{
			
			_interactiveObject = MakeInteractiveTrace();

			if (_interactiveObject != null)
			{
				if (_lastInteractive == null)
				{
					_interactiveObject.FocusEnter();
				}
				else
				{
					if (_lastInteractive != _interactiveObject)
					{
						_lastInteractive.FocusExit();

						_interactiveObject.FocusEnter();
					}
				}

				_lastInteractive = _interactiveObject;
			}
			else
			{
				if (_lastInteractive != null)
				{
					_lastInteractive.FocusExit();
				}

				_lastInteractive = null;
			}
		}

		//List<RaycastResult> results = new List<RaycastResult>();

/*		public void RaycastWorldUI()
		{
			PointerEventData data = new PointerEventData(EventSystem.current);
			data.position = new Vector2(Screen.width / 2, Screen.height / 2);
			//data.position = _ray.GetPoint(InteractionDistance);
			EventSystem.current.RaycastAll(data, results);

			foreach (var result in results)
			{
				//Debug.LogError("T:" + result.gameObject.name + " Dist:" + result.distance);

				if (result.distance <= InteractionUIDistance)
				{
					ExecuteEvents.ExecuteHierarchy<ISubmitHandler>(
						result.gameObject, data,
						ExecuteEvents.submitHandler
					);
				}
			}
		}*/


		public Interactive MakeInteractiveTrace()
		{
			_ray = RayCastCamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0)); //From Screen Center
			bool cast = Physics.Raycast(_ray, out _hitInfo, MaxInteractionDistance, InteractionLayerMask);
			Debug.DrawRay(_ray.origin, _ray.direction * _hitInfo.distance, Color.red);

			if (cast)
			{
				/*var ignore = ((1 << _hitInfo.collider.gameObject.gameObject.layer) & InteractionBlockLayerMask) != 0;
				if (ignore)
				{
					return null;
				}*/
				
				Interactive interactive = _hitInfo.collider.GetComponentInParent<Interactive>();
				/*//FOR DEBUGING
				if (interactive != null)
				{
					var angle = Quaternion.Angle(Camera.main.transform.rotation, (interactive as MonoBehaviour).transform.localRotation);
					Debug.LogError("angle:" + angle);
				}*/
				if (interactive != null && interactive.CanInteract(_owner, _hitInfo.distance))
				{
					Debug.DrawRay(_ray.origin, _ray.direction * _hitInfo.distance, Color.yellow);
					return interactive;
				}
			}

			return null;
		}
	}
}
