﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;


namespace AndrewNovak.InteractiveSystem
{
	[RequireComponent(typeof(Informative))]
	public class Interactive : MonoBehaviour
	{
		public event Action<BaseCharacter> Interact;
		
		public Action OnFocusEnter;
		public Action OnFocusExit;
		

		public InformativeInfo Info
		{
			get
			{
				if (!_informative)
				{
					_informative = GetComponent<Informative>();
				}

				if (_informative == null)
				{
					Debug.LogError($"Not Informative:{this.gameObject.name}", this);
				}
				else
				{
					if (this.name.ToLower().Contains("handle"))
					{
						
					}
				}
				return _informative.Info;
			}
		}
		
		public float InteractionDistance = 1.8f;
		public Vector2 InteractionAngleRange = new Vector2(0, 180);

		private Informative _informative;

		private Outline[] _outlines;
		private bool _interactionEnabled = true;

		private void Awake()
		{
			_outlines = GetComponentsInChildren<Outline>(true);
			foreach (var outline in _outlines)
			{
				outline.enabled = false;
			}
		}

		public void SetInteractionEnabled(bool interactionEnabled)
		{
			_interactionEnabled = interactionEnabled;
		}

		public bool CanInteract(BaseCharacter character, float distance)
		{
			var playerCharacter = character as PlayerCharacter;

			bool needAngle = false;
			
			if (playerCharacter)
			{
				var playerCamera = playerCharacter.GetComponentInChildren<Camera>();
				var angle = Quaternion.Angle(playerCamera.transform.rotation, transform.rotation);
				//Debug.LogError("interact angle:" + angle);
				needAngle = (angle > InteractionAngleRange.x && angle < InteractionAngleRange.y);
			}
			else
			{
				Debug.LogError($"SOME Other Character:{character.name} Try Interact with:{gameObject.name}", this);
			}
			
			return _interactionEnabled && needAngle && distance <= InteractionDistance && !character.IsGrabSomething();
		}

		public void OnInteract(BaseCharacter character)
		{
			Interact?.Invoke(character);
		}

		public float GetMass()
		{
			var container = GetComponent<IContainer>();
			if (container != null)
			{
				return container.Mass;
			}
			
			var rb = GetComponent<Rigidbody>();
			return rb ? rb.mass : 0f;
		}

		public bool InFocus { get; private set; }

		public void FocusEnter()
		{
			foreach (var outline in _outlines)
			{
				outline.enabled = true;
			}

			//_infoPanel.Show(this);

			InFocus = true;
			
			OnFocusEnter?.Invoke();
		}

		public void FocusExit()
		{
			foreach (var outline in _outlines)
			{
				outline.enabled = false;
			}

			//_infoPanel.Hide();

			InFocus = false;
			
			OnFocusExit?.Invoke();
		}
/*#if UNITY_EDITOR
		[ContextMenu("FillInfo")]
		public void FillInfo()
		{
			string newName = string.Format("IAI_{0}", gameObject.name.Remove(0, 2));

			var fResults = UnityEditor.AssetDatabase.FindAssets(newName);

			if (fResults.Length <= 0)
			{
				return;
			}

			var infoAsset = UnityEditor.AssetDatabase.LoadAssetAtPath<InformativeInfo>(UnityEditor.AssetDatabase.GUIDToAssetPath(fResults[0]));
			Info = infoAsset;
			UnityEditor.EditorUtility.SetDirty(this);
			UnityEditor.AssetDatabase.SaveAssets();
			UnityEditor.AssetDatabase.Refresh();
			
		}
#endif*/


		
	}
}
