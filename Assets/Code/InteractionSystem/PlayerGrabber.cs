﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Input;

namespace AndrewNovak.InteractiveSystem
{
	public class PlayerGrabber : MonoBehaviour
	{

		public Action<Interactive> OnGrab;
		public Action<Interactive> OnRelease;

		public Interactive GrabbedComponent => _grabbed;
		
		[SerializeField]
		protected Transform GrabPoint;
		[SerializeField]
		protected Vector3 GrabOffset;
		[SerializeField]
		protected float GrabForce = 600;
		[SerializeField]
		protected float GrabDamping = 35;
		[SerializeField]
		protected float ReleaseForceMultipler = 5;
		[SerializeField]
		protected float ReleaseDelay = 0.1f;
		
		[Header("[SFX]")]
		[SerializeField, BundleAssetReference]
		private string GrabSound;
		[SerializeField, BundleAssetReference]
		private string ReleaseSound;
		
		
		private Transform _jointTrans;
		private Interactive _grabbed;
		private float _grabDistance;

		private Vector3 _grabPointDefaultPos;
		private Vector3 _grabPointPos;
		private float _lastGrabTime;
		private AudioSource _grabSource;

		private void Awake()
		{
			_grabSource = GetComponentInChildren<AudioSource>();
			_grabPointDefaultPos = GrabPoint.localPosition;
			
			var player = GetComponent<PlayerCharacter>();
			player.Controls.Player.MainAction.performed += MainActionOnPerformed;
			player.Controls.Player.SecondAction.performed += SecondActionOnPerformed;
		}

		/*private void OnDestroy()
		{
						
			var player = GetComponent<PlayerCharacter>();
			player.Controls.Player.MainAction.performed -= MainActionOnPerformed;
			player.Controls.Player.SecondAction.performed -= SecondActionOnPerformed;
		}*/

		private void MainActionOnPerformed(InputAction.CallbackContext inputActionContext)
		{
			if (IsGrab)
			{
				var cam = GetComponentInChildren<Camera>();
				Release(cam.transform.forward * ReleaseForceMultipler);
				
			}
		}
		private void SecondActionOnPerformed(InputAction.CallbackContext inputActionContext)
		{
			Release(Vector3.zero);
		}


		private void Update()
		{
			GrabPoint.transform.localPosition = _grabPointPos + GrabOffset;
			
			if (_jointTrans != null)
			{
				_jointTrans.position = GrabPoint.transform.position;
				if (GrabbedComponent != null)
				{
					var distance = (GrabbedComponent.transform.position - GrabPoint.position).magnitude;
					if (distance > _grabDistance)
					{
						Release(Vector3.zero);
					}
				}
				
			}
		}


		public void Grab(Interactive grabbed)
		{
			if (IsGrab)
			{
				return;
			}

			_grabbed = grabbed;
			_grabDistance = (transform.position - grabbed.transform.position).magnitude;
			Debug.LogWarning($"[INTERACTION] Grab {grabbed.Info.Name} Distance:" + _grabDistance, grabbed);
			if (_grabDistance < 0.856f)
			{
				if (_grabDistance < 0.35f)
				{
					_grabPointPos = new Vector3(_grabPointPos.x, _grabPointPos.y, 0.45f);
				}
				else
				{
					_grabPointPos = new Vector3(_grabPointPos.x, _grabPointPos.y, _grabDistance);
				}
			}
			else
			{
				_grabPointPos =  new Vector3(_grabPointPos.x, _grabPointPos.y, 0.856f);
			}
			
			if (!_jointTrans)
			{
				_jointTrans = AttachJoint(_grabbed.GetComponent<Rigidbody>(), GrabPoint.position);

				if (_grabbed.GetComponent<IContainer>() != null)
				{
					_jointTrans.localRotation = Quaternion.identity;
				}
			}

			_lastGrabTime = Time.realtimeSinceStartup;

			PlayGrabSound();
			
			OnGrab?.Invoke(_grabbed);
		}


		public bool IsGrab => _grabbed;

		public void Release(Vector3 releaseForce)
		{
			var releaseTime = Time.realtimeSinceStartup;
			var waitReleaseDelay = (releaseTime - _lastGrabTime) >= ReleaseDelay;
			if (!IsGrab || !waitReleaseDelay)
			{
				return;
			}
			
			Destroy(_jointTrans.gameObject);
			
			_grabPointPos = _grabPointDefaultPos;

			var rb = _grabbed.GetComponent<Rigidbody>();
			rb.WakeUp();
			
			//Add force (if needed)
			if (releaseForce != Vector3.zero)
			{
				rb.angularVelocity = Vector3.zero;
				rb.AddForce(rb.velocity + (releaseForce * _grabbed.GetMass()), ForceMode.Impulse);
			}
			
			//Clear actor ref
			_grabbed = null;
			
			PlayReleaseSound();
			
			OnRelease?.Invoke(_grabbed);
		}

		Transform AttachJoint(Rigidbody rb, Vector3 attachmentPosition)
		{
			GameObject go = new GameObject("Attachment Point");
			//go.hideFlags = HideFlags.HideInHierarchy;
			go.transform.rotation = rb.transform.rotation;
			go.transform.position = attachmentPosition;

			var newRb = go.AddComponent<Rigidbody>();
			newRb.isKinematic = true;

			var joint = go.AddComponent<ConfigurableJoint>();
			
			joint.configuredInWorldSpace = true;
			joint.autoConfigureConnectedAnchor = false;
			joint.connectedAnchor = Vector3.zero;

			var interactive = rb.GetComponent<Interactive>();
			var grabForce = GrabForce + interactive.GetMass();
			joint.xDrive = NewJointDrive(grabForce, GrabDamping);
			joint.yDrive = NewJointDrive(grabForce, GrabDamping);
			joint.zDrive = NewJointDrive(grabForce, GrabDamping);
			joint.slerpDrive = NewJointDrive(grabForce, GrabDamping);
			joint.rotationDriveMode = RotationDriveMode.Slerp;
			joint.connectedBody = rb;
			return go.transform;
		}
		void Attach(Interactive interactive)
		{

			var newRb = GrabPoint.gameObject.GetComponent<Rigidbody>();
			newRb.isKinematic = true;

			var joint = GrabPoint.gameObject.GetComponent<ConfigurableJoint>();
			
			joint.configuredInWorldSpace = true;
			joint.autoConfigureConnectedAnchor = false;
			joint.connectedAnchor = Vector3.zero;

			var grabForce = GrabForce;//+ interactive.GetMass();
			joint.xDrive = NewJointDrive(grabForce, GrabDamping);
			joint.yDrive = NewJointDrive(grabForce, GrabDamping);
			joint.zDrive = NewJointDrive(grabForce, GrabDamping);
			joint.slerpDrive = NewJointDrive(grabForce, GrabDamping);
			joint.rotationDriveMode = RotationDriveMode.Slerp;
			joint.connectedBody = interactive.GetComponent<Rigidbody>();
		}

		private JointDrive NewJointDrive(float force, float damping)
		{
			JointDrive drive = new JointDrive();
			//drive.mode = JointDriveMode.Position;
			drive.positionSpring = force;
			drive.positionDamper = damping;
			drive.maximumForce = Mathf.Infinity;
			return drive;
		}

		private void PlayGrabSound()
		{
			UnityAudioPlayer.PlayOneShoot(GrabSound, _grabSource);
		}

		private void PlayReleaseSound()
		{
			UnityAudioPlayer.PlayOneShoot(ReleaseSound, _grabSource);
			
		}
	}
}
