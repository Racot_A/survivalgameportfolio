﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class ModelsReplacerWindow : EditorWindow
{


	public static GameObject[] SceneObjects;
	public static GameObject AssetObject;
	
	private int currentPickerWindow;
	
	[MenuItem("Window/Custom/Models Replacer")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		ModelsReplacerWindow window = (ModelsReplacerWindow)EditorWindow.GetWindow(typeof(ModelsReplacerWindow));
		window.Show();
	}

	void OnGUI()
	{
		SceneObjects = Selection.gameObjects;
		
		if (GUILayout.Button("Replace"))
		{
			PickObject();
		}
		
		if( Event.current.commandName == "ObjectSelectorClosed" && EditorGUIUtility.GetObjectPickerControlID() == currentPickerWindow )
		{        
			AssetObject = EditorGUIUtility.GetObjectPickerObject() as GameObject;
			currentPickerWindow = -1;

			if (SceneObjects == null || SceneObjects.Length <= 0 || AssetObject == null)
			{
				return;
			}

			foreach (var sceneObject in SceneObjects)
			{
				Replace(sceneObject, AssetObject);
			}
		}
	}

	public void PickObject()
	{
		EditorGUIUtility.ShowObjectPicker<GameObject>(Selection.activeGameObject, false, "t:GameObject", currentPickerWindow);
	}
	
	public static void Replace(GameObject sceneObject, GameObject assetObject)
	{
		if (sceneObject == null || assetObject == null)
		{
			Debug.LogError("sceneObject == null || assetObject == null");
			return;
		}
		
		if (sceneObject == assetObject)
		{
			Debug.LogError("sceneObject == assetObject");
			return;
		}
		
		var instObj = PrefabUtility.InstantiatePrefab(assetObject) as GameObject;
		if (instObj != null)
		{
			Undo.RegisterCreatedObjectUndo (instObj, "Created go");
			
			var ind = sceneObject.name.LastIndexOf(' ');
			if (ind >= 0)
			{
				var sceneObjectCuttedName = sceneObject.name.Remove(0, ind);
				instObj.name = assetObject.name + sceneObjectCuttedName;
				//Debug.LogError("Name:"  + assetObject.name + sceneObjectCuttedName);
			}

			instObj.transform.SetParent(sceneObject.transform.parent, true);
			instObj.transform.position = sceneObject.transform.position;
			instObj.transform.rotation = sceneObject.transform.rotation;
			instObj.transform.localScale = sceneObject.transform.localScale;
		
			Debug.LogError("Replaced Scene:" + sceneObject + " with Asset:" + assetObject, instObj);
			
			Undo.DestroyObjectImmediate (sceneObject.gameObject);
			Selection.activeGameObject = instObj;
			
			EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
			
			
		}
		

	}
}
